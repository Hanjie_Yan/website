<?php

class Pages extends CI_Controller {

	public function view($page = 'home')
{

	if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
	{
		// Whoops, we don't have a page for that!
		show_404();
	}

	$data['title'] = "Hanjie Yan"; // Capitalize the first letter

	$this->load->view('templates/header', $data);
	$this->load->view('pages/'.$page, $data);
	$this->load->view('templates/footer', $data);

}	

	public function contact()
{
	$data["sent"]="";
	$this->load->view('templates/header');
	$this->load->view('pages/pic.php',$data);
	$this->load->view('templates/footer');

}
	public function send(){
		$this->load->library("form_validation");

		$this->form_validation->set_rules("fullName", "Name", "trim|required|alpha|xss_clear|min_length[1]|max_length[12]");
		$this->form_validation->set_rules("email", "Email", "trim|required|valid_email|xss_clear");
		$this->form_validation->set_rules("message", "Message", "trim|required|xss_clear");

		if($this->form_validation->run() == FALSE){
			$data["sent"] = "";
			$this->load->view('templates/header');
			$this->load->view('pages/home', $data);
			$this->load->view('templates/footer');
		}

		else{
			$data["sent"] = "Sent!";

			$this ->load->library("email");
			$this ->email->from(set_value("email"), set_value("fullName"));
			$this ->email->to("yanhanjieinuk@gmail.com");
			$this ->email->subject("Message from out form");
			$this ->email->message(set_value("message"));

			$this ->email->send();

			echo $this->email->print_debugger();

			$this->load->view('templates/header');
			$this->load->view('pages/home', $data);
			$this->load->view('templates/footer');
		}
	}
}