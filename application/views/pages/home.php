<link rel='stylesheet prefetch' href='//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
<nav class="mainMenu">
    <div class="point">
        <div class="spinner">
            <div class="blob top"></div>
            <div class="blob bottom"></div>
            <div class="blob left"></div>
            <div class="blob move-blob"></div>
        </div>
    </div>

    <h3><br><br></h3>
    <ul class="mainMenu-links">
        <li><a href="#about" class="scroll active" data-target="about">About</a>
        </li>
        <li><a href="#portfolio" class="scroll" data-target="tete">Portfolio</a>
        </li>
        <li><a href="#skill" class="scroll" data-target="titi">skill</a>
        </li>
        <li><a href="#contact" class="scroll" data-target="toto">Contact</a>
        </li>
    </ul>
</nav>
<section class="sectionOnePage" id="about">
    <div class="container">
        <div class="sixteen columns">
            <div class="title">
                <div class="content">About  me</div>
            </div>
        </div>
        <div class="about-pic">
        <div class="three columns offset-by-six">
                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                        <div class="front">
                            <img src="<?php echo base_url();?>/resources/img/about-pic.jpg"/>
                        </div>
                        <div class="back">
                            <ul class="social">
                            <li class="facebook">
                                <a href="https://www.facebook.com/jeff.yan.1213" class="entypo-facebook"></a>
                            </li>
                            <li class="linked-in">
                                <a href="https://www.linkedin.com/hp/?dnr=X-Az3AxA9bXwc7A8o-tQMtbiLbXxBHzK6n5A" class="entypo-linkedin"></a>
                            </li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
        </div>
        <div class="sixteen columns"></div>
        <div class="about-txt">
            <div class="ten columns offset-by-four">
                <div class="hello">
                    <div class="three columns"><h3>Hello, I am</h3></div>
                </div>
                <div class="name">
                    <div class="four columns">
                        <nav class="cl-effect-19" id="cl-effect-19">
                            <a><span data-hover="a programmer">Hanjie Yan</span></a>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="ten columns offset-by-four">
                <div class="role">
                    <div class="eight columns">
                        <h4>I'm a Web developer, Software Engineer</h4>
                    </div>   
                </div>
            </div>
            <div class="ten columns offset-by-four">
                <div class="txt">
                    <div class="nine columns">
                        <h5>Nice to meet you! I'm currently studying in Leeds Buckett University for the final year. Web developing and software programming <br>are my hobbies!</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="one columns offset-by-seven">
            <div class="down">
                <a class="arrow scroll" href="#portfolio"><span> </span></a>
            </div>
        </div>
</section>



<section class="sectionOnePage" id="portfolio">
    <div class="container">
        <div class="sixteen columns">
            <div class="title">
                <div class="content">portfolio</div>
            </div>
        </div>
        <div class="portfolio">
            <div class="sixteen columns">
                <div class="portfolio-filters">
                    <ul id="filters" class="clearfix">
                        <li>
                            <span class="filter active" onclick="setBigClass" data-filter="website java c#">All</span>
                        </li>
                        <li>
                            <span class="filter" onclick="setSmallClass"data-filter="website">Website</span>
                        </li>
                        <li>
                            <span class="filter" data-filter="java">Java</span>
                        </li>
                        <li>
                            span class="filter" data-filter="c#">C#</span>
                        </li>
                    </ul>
                </div>
            </div>
                        <div id="portfoliolist">
                            <div class="list website" data-cat="website">
                                <div class="five columns">
                                <div class="portfolio-wrapper">
                                    <div class="fancyDemo">
                                        <div class="photoset wrap">
                                            <img src="<?php echo base_url();?>/resources/img/3d-mac.png" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/3d1.jpg" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/3d2.jpg" alt=""class="img-responsive"/>
                                        </div>
                                        <h4><a href="http://yams.me.uk/3d/">Impress</a></h4>
                                    </div>
                                    <div class="label">
                                        <p>3D objects/graphs develop on Html5 and WebGL
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="list java" data-cat="java">
                                <div class="five columns">
                                <div class="portfolio-wrapper">
                                    <div class="fancyDemo">
                                        <div class="photoset wrap">
                                            <img src="<?php echo base_url();?>/resources/img/draw-mac.png" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/draw1.jpg" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/draw2.jpg" alt=""class="img-responsive"/>
                                            
                                        </div>
                                        <h4><a href="https://github.com/yanhanjie/drawsomethings">Draw Somethings</a></h4>
                                    </div>
                                    <div class="label">
                                        <div class="label-text">
                                            <p>Java programm, Command line drawing on java(Available at GitHub)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="list c#" data-cat="c#">
                                <div class="five columns">
                                <div class="portfolio-wrapper">
                                    <div class="fancyDemo">
                                        <div class="photoset wrap">
                                            <img src="<?php echo base_url();?>/resources/img/cycling-mac.png" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/cycling2.jpg" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/cycling1.jpg" alt=""class="img-responsive"/>
                                        </div>
                                        <h4><a href="https://github.com/yanhanjie/Cycling">Cycling</a></h4>
                                    </div>
                                    <div class="label">
                                        <div class="label-text">
                                            <p>C# program,designed for Polar sports,finally year programm(Available at GitHub)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="list website" data-cat="website">
                                <div class="five columns">
                                <div class="portfolio-wrapper">
                                    <div class="fancyDemo">
                                        <div class="photoset wrap">
                                            <img src="<?php echo base_url();?>/resources/img/aida-mac.png" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/aida1.jpg" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/aida2.jpg" alt=""class="img-responsive"/>
                                        </div>
                                        <h4><a href="http://yams.me.uk/aida/">AIDA</a></h4>
                                    </div>
                                    <div class="label">
                                        <div class="label-text">
                                            <p>Web servies and API plug ins, finally year assignment</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="list java" data-cat="java">
                                <div class="five columns">
                                <div class="portfolio-wrapper">
                                    <div class="fancyDemo">
                                        <div class="photoset wrap">
                                            <img src="<?php echo base_url();?>/resources/img/guess-mac.png" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/guessing1.jpg" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/guessing2.jpg" alt=""class="img-responsive"/>
                                        </div>
                                        <h4><a href="https://github.com/yanhanjie/guessing_game">Guessing</a></h4>
                                    </div>
                                    <div class="label">
                                        <div class="label-text">
                                            <p>A android software, guessing number to win the game!(Available at GitHub)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="list java" data-cat="java">
                                <div class="five columns">
                                <div class="portfolio-wrapper">
                                    <div class="fancyDemo">
                                        <div class="photoset wrap">
                                            <img src="<?php echo base_url();?>/resources/img/spirit-mac.png" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/spirit1.jpg" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/spirit2.jpg" alt=""class="img-responsive"/>
                                        </div>
                                        <h4><a href="https://github.com/yanhanjie/spirit">Spirit</a></h4>
                                    </div>
                                    <div class="label">
                                        <div class="label-text">
                                            <p>Android Game, catch as much spirits as you can!(Available at GitHub)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="list c#" data-cat="c#">
                                <div class="five columns">
                                <div class="portfolio-wrapper">
                                    <div class="fancyDemo">
                                        <div class="photoset wrap">
                                            <img src="<?php echo base_url();?>/resources/img/fractal-mac.png" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/fractal1.jpg" alt=""class="img-responsive"/>
                                            <img src="<?php echo base_url();?>/resources/img/fractal2.jpg" alt=""class="img-responsive"/>
                                        </div>
                                        <h4><a href="https://github.com/yanhanjie/fractal">Fractal Draw</a></h4>
                                    </div>
                                    <div class="label">
                                        <div class="label-text">
                                            <p>C# programm, drawing a amazing Fractal graph(Available at GitHub)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>         
        </div>
    </div>  
            <div class="sixteen columns"></div>
            <div class="one columns offset-by-seven">
            <div class="down">
                <a class="arrow scroll" href="#skill"><span> </span></a>
            </div>
        </div>
</section>






<section class="sectionOnePage" id="skill">
    <div class="container">
        <div class="sixteen columns">
            <div class="title">
                <div class="content">skill</div>
            </div>
        </div>
        <div class="four columns offset-by-six">
            <div class='pulse'></div>
        </div>
        <div class="sixteen columns"></div>
        <div class="six columns offset-by-four">
        <div class="skills-resume">
            <div class="rgt">
                <div class="graph">
                    <p class="keywords">
                        <b class="keyword webgl">WebGL</b>
                        <b class="keyword html-css">HTML5 / CSS3</b>
                        <b class="keyword reference-naturel">Responsive<br>Design</b>
                        <b class="keyword csharp">C#</b>
                        <b class="keyword webdesign">WebDesign</b>
                        <b class="keyword jquery-javascript">jQuery<br>Javascript</b>
                        <b class="keyword php">PHP</b>
                        <b class="keyword Java">Java</b>
                    </p>
                </div>
            </div>
        </div>
        </div>
        <div class="sixteen columns"></div>
        <div class="eight columns">
        <div class="idesign">
            <h5>I Design</h5>
        </div>  
            <div id="design">
                <div id="demo" style="height: 240px; width: 240px; margin-left: -120px; margin-top: -120px;">
                <div class="d" style="width: 7px; height: 29px; left: 11px; top: 7px;"><a style="height: 61px;"><b style="width: 29px;"><b style="width: 7px;"><b style="width: 29px;"><i style="width: 29px; height: 7px;"></i></b></b></b></a></div>
                <div class="a" style="width: 18px; height: 31px; left: 9px; top: 48px;"><a style="height: 41px;"><b style="width: 31px;"><b style="width: 18px;"><b style="width: 31px;"><i style="width: 31px; height: 18px;"></i></b></b></b></a></div>
                <div class="b" style="width: 28px; height: 7px; left: 8px; top: 96px;"><a style="height: 73px;"><b style="width: 7px;"><b style="width: 28px;"><b style="width: 7px;"><i style="width: 7px; height: 28px;"></i></b></b></b></a></div>
                <div class="c" style="width: 18px; height: 17px; left: 22px; top: 128px;"><a style="height: 15px;"><b style="width: 17px;"><b style="width: 18px;"><b style="width: 17px;"><i style="width: 17px; height: 18px;"></i></b></b></b></a></div>
                <div class="b" style="width: 18px; height: 30px; left: 8px; top: 164px;"><a style="height: 64px;"><b style="width: 30px;"><b style="width: 18px;"><b style="width: 30px;"><i style="width: 30px; height: 18px;"></i></b></b></b></a></div>
                <div class="b" style="width: 20px; height: 26px; left: 11px; top: 213px;"><a style="height: 32px;"><b style="width: 26px;"><b style="width: 20px;"><b style="width: 26px;"><i style="width: 26px; height: 20px;"></i></b></b></b></a></div>
                <div class="d" style="width: 23px; height: 32px; left: 53px; top: 6px;"><a style="height: 64px;"><b style="width: 32px;"><b style="width: 23px;"><b style="width: 32px;"><i style="width: 32px; height: 23px;"></i></b></b></b></a></div>
                <div class="c" style="width: 14px; height: 15px; left: 55px; top: 63px;"><a style="height: 71px;"><b style="width: 15px;"><b style="width: 14px;"><b style="width: 15px;"><i style="width: 15px; height: 14px;"></i></b></b></b></a></div>
                <div class="b" style="width: 17px; height: 33px; left: 50px; top: 86px;"><a style="height: 63px;"><b style="width: 33px;"><b style="width: 17px;"><b style="width: 33px;"><i style="width: 33px; height: 17px;"></i></b></b></b></a></div>
                <div class="e" style="width: 8px; height: 18px; left: 65px; top: 129px;"><a style="height: 69px;"><b style="width: 18px;"><b style="width: 8px;"><b style="width: 18px;"><i style="width: 18px; height: 8px;"></i></b></b></b></a></div>
                <div class="d" style="width: 30px; height: 18px; left: 45px; top: 171px;"><a style="height: 44px;"><b style="width: 18px;"><b style="width: 30px;"><b style="width: 18px;"><i style="width: 18px; height: 30px;"></i></b></b></b></a></div>
                <div class="c" style="width: 13px; height: 15px; left: 52px; top: 213px;"><a style="height: 12px;"><b style="width: 15px;"><b style="width: 13px;"><b style="width: 15px;"><i style="width: 15px; height: 13px;"></i></b></b></b></a></div>
                <div class="c" style="width: 9px; height: 27px; left: 84px; top: 7px;"><a style="height: 59px;"><b style="width: 27px;"><b style="width: 9px;"><b style="width: 27px;"><i style="width: 27px; height: 9px;"></i></b></b></b></a></div>
                <div class="e" style="width: 15px; height: 21px; left: 97px; top: 58px;"><a style="height: 66px;"><b style="width: 21px;"><b style="width: 15px;"><b style="width: 21px;"><i style="width: 21px; height: 15px;"></i></b></b></b></a></div>
                <div class="d" style="width: 29px; height: 13px; left: 88px; top: 107px;"><a style="height: 31px;"><b style="width: 13px;"><b style="width: 29px;"><b style="width: 13px;"><i style="width: 13px; height: 29px;"></i></b></b></b></a></div>
                <div class="c" style="width: 7px; height: 28px; left: 104px; top: 128px;"><a style="height: 47px;"><b style="width: 28px;"><b style="width: 7px;"><b style="width: 28px;"><i style="width: 28px; height: 7px;"></i></b></b></b></a></div>
                <div class="b" style="width: 9px; height: 14px; left: 88px; top: 177px;"><a style="height: 54px;"><b style="width: 14px;"><b style="width: 9px;"><b style="width: 14px;"><i style="width: 14px; height: 9px;"></i></b></b></b></a></div>
                <div class="c" style="width: 20px; height: 8px; left: 99px; top: 216px;"><a style="height: 19px;"><b style="width: 8px;"><b style="width: 20px;"><b style="width: 8px;"><i style="width: 8px; height: 20px;"></i></b></b></b></a></div>
                <div class="e" style="width: 16px; height: 15px; left: 132px; top: 18px;"><a style="height: 42px;"><b style="width: 15px;"><b style="width: 16px;"><b style="width: 15px;"><i style="width: 15px; height: 16px;"></i></b></b></b></a></div>
                <div class="b" style="width: 14px; height: 10px; left: 137px; top: 59px;"><a style="height: 43px;"><b style="width: 10px;"><b style="width: 14px;"><b style="width: 10px;"><i style="width: 10px; height: 14px;"></i></b></b></b></a></div>
                <div class="c" style="width: 34px; height: 16px; left: 125px; top: 101px;"><a style="height: 62px;"><b style="width: 16px;"><b style="width: 34px;"><b style="width: 16px;"><i style="width: 16px; height: 34px;"></i></b></b></b></a></div>
                <div class="e" style="width: 21px; height: 11px; left: 137px; top: 124px;"><a style="height: 36px;"><b style="width: 11px;"><b style="width: 21px;"><b style="width: 11px;"><i style="width: 11px; height: 21px;"></i></b></b></b></a></div>
                <div class="b" style="width: 6px; height: 31px; left: 125px; top: 168px;"><a style="height: 13px;"><b style="width: 31px;"><b style="width: 6px;"><b style="width: 31px;"><i style="width: 31px; height: 6px;"></i></b></b></b></a></div>
                <div class="d" style="width: 25px; height: 30px; left: 133px; top: 208px;"><a style="height: 77px;"><b style="width: 30px;"><b style="width: 25px;"><b style="width: 30px;"><i style="width: 30px; height: 25px;"></i></b></b></b></a></div>
                <div class="c" style="width: 32px; height: 11px; left: 163px; top: 11px;"><a style="height: 30px;"><b style="width: 11px;"><b style="width: 32px;"><b style="width: 11px;"><i style="width: 11px; height: 32px;"></i></b></b></b></a></div>
                <div class="e" style="width: 26px; height: 13px; left: 168px; top: 55px;"><a style="height: 27px;"><b style="width: 13px;"><b style="width: 26px;"><b style="width: 13px;"><i style="width: 13px; height: 26px;"></i></b></b></b></a></div>
                <div class="e" style="width: 30px; height: 13px; left: 164px; top: 90px;"><a style="height: 78px;"><b style="width: 13px;"><b style="width: 30px;"><b style="width: 13px;"><i style="width: 13px; height: 30px;"></i></b></b></b></a></div>
                <div class="c" style="width: 32px; height: 29px; left: 163px; top: 129px;"><a style="height: 48px;"><b style="width: 29px;"><b style="width: 32px;"><b style="width: 29px;"><i style="width: 29px; height: 32px;"></i></b></b></b></a></div>
                <div class="e" style="width: 10px; height: 29px; left: 183px; top: 164px;"><a style="height: 41px;"><b style="width: 29px;"><b style="width: 10px;"><b style="width: 29px;"><i style="width: 29px; height: 10px;"></i></b></b></b></a></div>
                <div class="a" style="width: 16px; height: 20px; left: 164px; top: 219px;"><a style="height: 63px;"><b style="width: 20px;"><b style="width: 16px;"><b style="width: 20px;"><i style="width: 20px; height: 16px;"></i></b></b></b></a></div>
                <div class="e" style="width: 8px; height: 31px; left: 217px; top: 4px;"><a style="height: 37px;"><b style="width: 31px;"><b style="width: 8px;"><b style="width: 31px;"><i style="width: 31px; height: 8px;"></i></b></b></b></a></div>
                <div class="d" style="width: 29px; height: 9px; left: 209px; top: 53px;"><a style="height: 48px;"><b style="width: 9px;"><b style="width: 29px;"><b style="width: 9px;"><i style="width: 9px; height: 29px;"></i></b></b></b></a></div>
                <div class="b" style="width: 24px; height: 16px; left: 211px; top: 84px;"><a style="height: 38px;"><b style="width: 16px;"><b style="width: 24px;"><b style="width: 16px;"><i style="width: 16px; height: 24px;"></i></b></b></b></a></div>
                <div class="c" style="width: 7px; height: 12px; left: 204px; top: 124px;"><a style="height: 78px;"><b style="width: 12px;"><b style="width: 7px;"><b style="width: 12px;"><i style="width: 12px; height: 7px;"></i></b></b></b></a></div>
                <div class="c" style="width: 12px; height: 16px; left: 204px; top: 164px;"><a style="height: 47px;"><b style="width: 16px;"><b style="width: 12px;"><b style="width: 16px;"><i style="width: 16px; height: 12px;"></i></b></b></b></a></div>
                <div class="c" style="width: 29px; height: 27px; left: 210px; top: 206px;"><a style="height: 72px;"><b style="width: 27px;"><b style="width: 29px;"><b style="width: 27px;"><i style="width: 27px; height: 29px;"></i></b></b></b></a></div>
                </div>
            </div>
        </div>

        <div class="six columns">
            <div class="icode">
                <h5>I Code</h5>
            </div>
            <div>
            <div class="code-editor">
                    <img src="<?php echo base_url();?>/resources/img/code-editor.png">
            </div>
            </div>
        </div>

            <div class="sixteen columns"></div>
            <div class="one columns offset-by-seven">
                <div class="down">
                    <a class="arrow scroll" href="#contact"><span> </span></a>
                </div>
            </div>
    </div>        
</section>

<section class="sectionOnePage" id="contact">
    <div class="container">
        <div class="sixteen columns">
            <div class="title">
                <div class="content">contact</div>
            </div>
        </div>
        <div class="three columns offset-by-three">
    <a href="https://www.facebook.com/jeff.yan.1213" target="_blank">
        <div class="iconn">
            <svg viewBox="0 0 400 400">
                <g id="linkedin">
                    <path d="M400,243.2v147.9h-85.7v-138c0-34.7-12.4-58.3-43.4-58.3c-23.7,0-37.8,15.9-44,31.4c-2.3,5.5-2.8,13.2-2.8,20.9v144h-85.8c0,0,1.2-233.7,0-257.9H224v36.6c-0.2,0.3-0.4,0.6-0.6,0.8h0.6v-0.8c11.4-17.5,31.7-42.6,77.3-42.6C357.7,127.2,400,164,400,243.2z M48.5,8.9C19.2,8.9,0,28.1,0,53.4C0,78.2,18.6,98,47.4,98H48c29.9,0,48.5-19.8,48.5-44.6C95.9,28.1,77.9,8.9,48.5,8.9z M5.1,391.1h85.7V133.2H5.1V391.1z"
                    class="shadow"></path>
                    <path d="M400,243.2v147.9h-85.7v-138c0-34.7-12.4-58.3-43.4-58.3c-23.7,0-37.8,15.9-44,31.4c-2.3,5.5-2.8,13.2-2.8,20.9v144h-85.8c0,0,1.2-233.7,0-257.9H224v36.6c-0.2,0.3-0.4,0.6-0.6,0.8h0.6v-0.8c11.4-17.5,31.7-42.6,77.3-42.6C357.7,127.2,400,164,400,243.2z M48.5,8.9C19.2,8.9,0,28.1,0,53.4C0,78.2,18.6,98,47.4,98H48c29.9,0,48.5-19.8,48.5-44.6C95.9,28.1,77.9,8.9,48.5,8.9z M5.1,391.1h85.7V133.2H5.1V391.1z"></path>
                </g>
            </svg>
        </div>
    </a>
        
    <a href="https://www.linkedin.com/hp/?dnr=X-Az3AxA9bXwc7A8o-tQMtbiLbXxBHzK6n5A" target="_blank">
        <div class="iconn">
            <svg viewBox="0 0 400 400">
                <g id="facebook">
                    <path d="M278.4,400V217.5h61.2l9.2-71.1h-70.4V101c0-20.6,5.7-34.6,35.2-34.6l37.7,0V2.8C344.8,1.9,322.5,0,296.5,0C242.2,0,205,33.1,205,94v52.4h-61.4v71.1H205V400H278.4z" class="shadow"></path>
                    <path d="M278.4,400V217.5h61.2l9.2-71.1h-70.4V101c0-20.6,5.7-34.6,35.2-34.6l37.7,0V2.8C344.8,1.9,322.5,0,296.5,0C242.2,0,205,33.1,205,94v52.4h-61.4v71.1H205V400H278.4z"></path>
                </g>
            </svg>
        </div>
    </a>
    </div> 
        <div class="contactForm">
            <div class="five columns">
            <?php
                $this->load->helper("form");

                echo $sent="";

                echo validation_errors();

                echo form_open("send#contact","class=contact-form");

                $nameattributes = array(
                    'class' => 'dashicons dashicons-universal-access',
                );
                ?>
                <div class="labelInput">
                <?php
                echo form_label("", "fullName",$nameattributes);
                $data = array(
                    "name" => "fullName",
                    "id" => "fullName",
                    "placeholder" =>"Name",
                    "value" => set_value("fullName")
                );
                echo form_input($data);
                ?>
                </div>
                <div class="labelInput">
                <?
                $emailattributes = array(
                    'class' => 'dashicons dashicons-email-alt',
                );
                echo form_label("", "email",$emailattributes);
                $data = array(
                    "name" => "email",
                    "id" => "email",
                    "placeholder" =>"Email",
                    "value" => set_value("email")
                );
                echo form_input($data);
                ?>
                </div>
                <div class="labelInput">
                <?php
                $messageattributes = array(
                    'class' => 'dashicons dashicons-editor-quote',
                );
                echo form_label("", "message",$messageattributes);
                $data = array(
                    "name" => "message",
                    "id" => "email",
                    "placeholder" =>"Message",
                    "value" => set_value("message")
                );
                echo form_textarea($data);
                ?>
                </div>
                <?php
                echo form_submit("contactSumbit","send","class=sendButton");
                ?>
                <?php
                echo form_close();
                ?>
            </div>
        </div>
            <div class="one columns offset-by-seven">
                <div class="topp">
                    <a class="arrow scroll" href="#about"><span> </span></a>
                </div>
            </div>
    </div>

</section>







