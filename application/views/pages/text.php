<link rel='stylesheet prefetch' href='//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
<nav class="mainMenu">
    <div class="point">
        <div class="spinner">
            <div class="blob top"></div>
            <div class="blob bottom"></div>
            <div class="blob left"></div>
            <div class="blob move-blob"></div>
        </div>
    </div>

    <h3><br><br></h3>
    <ul class="mainMenu-links">
        <li><a href="#about" class="scroll active" data-target="about">About</a>
        </li>
        <li><a href="#portfolio" class="scroll" data-target="tete">Portfolio</a>
        </li>
        <li><a href="#skill" class="scroll" data-target="titi">skill</a>
        </li>
        <li><a href="#contact" class="scroll" data-target="toto">about</a>
        </li>
    </ul>
</nav>
<section class="sectionOnePage" id="about">
    <div class="container">
        <div class="sixteen columns">
            <div class="title">
                <div class="content">About  me</div>
            </div>
        </div>
 		<div class="about-pic">
    	<div class="three columns offset-by-six">
    		    <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                        <div class="front">
                            <img src="<?php echo base_url();?>/resources/img/about-pic.jpg"/>
                        </div>
                        <div class="back">
                            <ul class="social">
                            <li class="facebook">
                                <a href="https://www.facebook.com/jeff.yan.1213" class="entypo-facebook"></a>
                            </li>
                            <li class="linked-in">
                                <a href="https://www.linkedin.com/hp/?dnr=X-Az3AxA9bXwc7A8o-tQMtbiLbXxBHzK6n5A" class="entypo-linkedin"></a>
                            </li>
                            </ul>
                        </div>
                    </div>
                </div>
    	</div>
    	</div>
		<div class="sixteen columns"></div>
		<div class="about-txt">
			<div class="ten columns offset-by-four">
				<div class="hello">
                    <div class="three columns"><h3>Hello, I am</h3></div>
                </div>
               	<div class="name">
                    <div class="four columns">
                        <nav class="cl-effect-19" id="cl-effect-19">
                            <a><span data-hover="a programmer">Hanjie Yan</span></a>
                        </nav>
                    </div>
                </div>
    		</div>
    		<div class="ten columns offset-by-four">
            	<div class="role">
                    <div class="eight columns">
                        <h4>I'm a Web developer, Software Engineer</h4>
                    </div>   
				</div>
			</div>
			<div class="ten columns offset-by-four">
				<div class="txt">
                    <div class="nine columns">
                        <h5>Nice to meet you! I'm currently studying in Leeds Buckett University for the final year. Web developing and software programming <br>are my hobbies!</h5>
                    </div>
                </div>
			</div>
		</div>
</section>



<section class="sectionOnePage" id="portfolio">
    <div class="container">
        <div class="sixteen columns">
            <div class="title">
                <div class="content">portfolio</div>
            </div>
        </div> 
    </div>
</section>

<section class="sectionOnePage" id="skill">
    <div class="container">
        <div class="sixteen columns">
            <div class="title">
                <div class="content">skill</div>
            </div>
        </div> 
    </div>
</section>

<section class="sectionOnePage" id="contact">
    <div class="container">
        <div class="sixteen columns">
            <div class="title">
                <div class="content">contact</div>
            </div>
        </div> 
    </div>
</section>
<div class="backToTop">
    <a href="#"><i class="fa fa-arrow-up"></i></a>
</div>
