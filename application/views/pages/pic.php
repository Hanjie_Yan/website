<section class="sectionOnePage" id="contact">
    <div class="container">
        <div class="sixteen columns">
            <div class="title">
                <div class="content">contact</div>
            </div>
        </div> 
        <div class="eight columns">
            <?php
                $this->load->helper("form");

                echo $sent="";

                echo validation_errors();

                echo form_open("send");

                echo form_label("Name: ", "fullName");
                $data = array(
                    "name" => "fullName",
                    "id" => "fullName",
                    "value" => set_value("fullName")
                );
                echo form_input($data);

                echo form_label("Email: ", "email");
                $data = array(
                    "name" => "email",
                    "id" => "email",
                    "value" => set_value("email")
                );
                echo form_input($data);

                echo form_label("Message: ", "message");
                $data = array(
                    "name" => "message",
                    "id" => "email",
                    "value" => set_value("message")
                );
                echo form_textarea($data);

                echo form_submit("contactSumbit","send");

                echo form_close();
            ?>
        </div>
    </div>
</section>