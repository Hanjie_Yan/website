</body>
<footer>
	<script>var D = {

    base: 6,
    size: 40,
    space: 6,
    height: {
        min: 10,
        max: 80
    },
    type: ['a', 'b', 'c', 'd', 'e'],
    bldg: '<a><b><b><b><i></i></b></b></b></a>',

    Random: function(min, max) {
        var value = Math.random() * (max - min) + min;
        return Math.round(value);
    },


    Build: function() {
        for (var x = 0; x < D.base; x++) {
            for (var y = 0; y < D.base; y++) {

                var e = D.data[x * D.base + y];
                var w = D.Random(D.space, D.size - D.space);
                var h = D.Random(D.space, D.size - D.space);
                var l = D.Random(D.space / 2, D.size - w);
                var t = D.Random(D.space / 2, D.size - h);
                var z = D.Random(D.height.min, D.height.max);
                var i = D.Random(0, D.type.length - 1);
                var c = e.getElementsByTagName('*');

                e.className = D.type[i];

                e.style.width = w + 'px';
                e.style.height = h + 'px';
                e.style.left = x * D.size + l + 'px';
                e.style.top = y * D.size + t + 'px';

                c[0].style.height = z + 'px';
                c[1].style.width = h + 'px';
                c[2].style.width = w + 'px';
                c[3].style.width = h + 'px';
                c[4].style.width = h + 'px';
                c[4].style.height = w + 'px';
            }
        }
    },

    Create: function() {
        for (var i = 0; i < D.base * D.base; i++) {
            var div = document.createElement('div');
            div.innerHTML = D.bldg;
            D.demo.appendChild(div);
        }
        D.data = D.demo.getElementsByTagName('div');
    },

    Init: function() {
        var l = D.base * D.size;
        D.demo = document.getElementById('demo');
        D.demo.style.width = D.demo.style.height = l + 'px';
        D.demo.style.marginTop = D.demo.style.marginLeft = -l / 2 + 'px';
        D.demo.addEventListener('click', D.Build, false);
    },

    Run: function() {
        D.Init();
        D.Create();
        D.Build();
    }

};

D.Run();
</script>


</footer>
</html>